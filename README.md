go-contentencoding
------------------

go-contentencoding is a simple and fast Go library for content negotiation with
a small api, zero allocations, benchmarks and high test coverage.

This library was initially created to provide a fast alternative to the content
negotiation logic in GitLab Pages and it should be considered feature-complete.

It tracks the latest supported go versions (see https://go.dev/doc/devel/release#policy 
and the `go.mod` file).
