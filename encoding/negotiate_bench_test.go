package encoding_test

import (
	"testing"

	"gitlab.com/feistel/go-contentencoding/encoding"
)

func BenchmarkParse(b *testing.B) {
	benchmarks := []struct {
		name   string
		accept string
	}{
		{
			name:   "simple",
			accept: "br",
		},
		{
			name:   "value",
			accept: "gzip;q=1.0, br;q=0.5",
		},
		{
			name:   "big",
			accept: "gzip;q=1.0, identity; q=0.5, *;q=0",
		},
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				_, _ = encoding.ParseHeader(bm.accept)
			}
		})
	}
}

func BenchmarkNegotiate(b *testing.B) {
	pref := encoding.Preference{
		encoding.Brotli:   encoding.Weight(1),
		encoding.Gzip:     encoding.Weight(0.5),
		encoding.Identity: encoding.Weight(0),
	}

	benchmarks := []struct {
		name     string
		accept   string
		strategy encoding.WildcardResolutionStrategy
	}{
		{
			name:   "simple",
			accept: "br",
		},
		{
			name:   "firefox",
			accept: "gzip, deflate, br",
		},
		{
			name:   "value",
			accept: "gzip;q=1.0, br;q=0.5",
		},
		{
			name:   "big",
			accept: "gzip;q=1.0, identity; q=0.5, *;q=0",
		},
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				_, _ = pref.Negotiate(bm.accept, bm.strategy)
			}
		})
	}
}
