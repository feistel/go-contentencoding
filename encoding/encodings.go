package encoding

// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Encoding
const (
	Gzip     = "gzip"
	Compress = "compress"
	Deflate  = "deflate"
	Brotli   = "br"
	Identity = "identity"
	Any      = "*"
)
