package encoding_test

import (
	"fmt"
	"testing"

	"gitlab.com/feistel/go-contentencoding/encoding"
)

//nolint:gochecknoglobals // global preferences to avoid duplicate across tests
var (
	brGzipIdentityPref = encoding.Preference{
		encoding.Brotli:   encoding.Weight(1),
		encoding.Gzip:     encoding.Weight(0.7),
		encoding.Identity: encoding.Weight(0.1),
	}
	brGzipIdentityZeroPref = encoding.Preference{
		encoding.Brotli:   encoding.Weight(1),
		encoding.Gzip:     encoding.Weight(0.4),
		encoding.Identity: encoding.Weight(0),
	}
	brGzipPref = encoding.Preference{
		encoding.Brotli: encoding.Weight(1),
		encoding.Gzip:   encoding.Weight(0.2),
	}
	brIdentityZeroPref = encoding.Preference{
		encoding.Brotli:   encoding.Weight(1),
		encoding.Identity: encoding.Weight(0),
	}
)

func TestNegotiate(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		accept        string
		expectedError error
		available     encoding.Preference
		expect        []string
	}{
		{
			accept:        "",
			available:     brGzipIdentityPref,
			expectedError: encoding.ErrEmptyHeader,
		},
		{
			accept:    "br",
			available: brGzipIdentityPref,
			expect:    []string{encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "br",
			available: brGzipPref,
			expect:    []string{encoding.Brotli},
		},
		{
			accept:    "gzip, br",
			available: brGzipIdentityPref,
			expect:    []string{encoding.Brotli, encoding.Gzip, encoding.Identity},
		},
		{
			accept:    "gzip, br",
			available: brGzipPref,
			expect:    []string{encoding.Brotli, encoding.Gzip},
		},
		{
			accept:    "gzip, br",
			available: brGzipIdentityZeroPref,
			expect:    []string{encoding.Brotli, encoding.Gzip},
		},
		{
			accept:    "gzip, br",
			available: brIdentityZeroPref,
			expect:    []string{encoding.Brotli},
		},
		{
			accept:    "gzip;q=1.0, br;q=0.5",
			available: brGzipIdentityPref,
			expect:    []string{encoding.Gzip, encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "gzip;q=1.0, br;q=0.5",
			available: brGzipPref,
			expect:    []string{encoding.Gzip, encoding.Brotli},
		},
		{
			accept:    "gzip;q=0, br;q=0.5",
			available: brGzipIdentityPref,
			expect:    []string{encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "gzip;q=1.0, identity; q=0.5, *;q=0",
			available: brGzipIdentityPref,
			expect:    []string{encoding.Gzip, encoding.Identity},
		},
		{
			accept:        "gzip;q=foo, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
		{
			accept:        "gzip;foo, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
	}
	for _, tc := range testCases {
		tc := tc
		name := fmt.Sprintf("accept:(%s) | available:%v", tc.accept, tc.available)
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := tc.available.Negotiate(tc.accept, encoding.None)
			ErrorIs(t, err, tc.expectedError)
			DeepEqual(t, tc.expect, got)
		})
	}
}

func TestWildcardStrategy(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		accept    string
		available encoding.Preference
		expect    []string
		strategy  encoding.WildcardResolutionStrategy
	}{
		{
			accept:    "*",
			available: brGzipIdentityPref,
			strategy:  encoding.UseServerPref,
			expect:    []string{encoding.Brotli, encoding.Gzip, encoding.Identity},
		},
		{
			accept:    "*",
			available: brGzipIdentityPref,
			strategy:  encoding.None,
			expect:    []string{encoding.Any},
		},
		{
			accept:    "*",
			available: brGzipIdentityPref,
			strategy:  encoding.AliasIdentity,
			expect:    []string{encoding.Identity},
		},
		{
			accept:    "*",
			available: brGzipPref,
			strategy:  encoding.AliasIdentity,
			expect:    []string{},
		},
		{
			accept:    "br",
			available: brGzipIdentityPref,
			strategy:  encoding.UseServerPref,
			expect:    []string{encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "br",
			available: brGzipIdentityPref,
			strategy:  encoding.None,
			expect:    []string{encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "br",
			available: brGzipIdentityPref,
			strategy:  encoding.AliasIdentity,
			expect:    []string{encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "br, *",
			available: brGzipIdentityPref,
			strategy:  encoding.UseServerPref,
			expect:    []string{encoding.Brotli, encoding.Gzip, encoding.Identity},
		},
		{
			accept:    "br, *",
			available: brGzipIdentityPref,
			strategy:  encoding.None,
			expect:    []string{encoding.Brotli, encoding.Any},
		},
		{
			accept:    "br, *",
			available: brGzipIdentityPref,
			strategy:  encoding.AliasIdentity,
			expect:    []string{encoding.Brotli, encoding.Identity},
		},
		{
			accept:    "gzip;q=1.0, identity; q=0.5, *;q=0",
			available: brGzipPref,
			strategy:  encoding.UseServerPref,
			expect:    []string{encoding.Gzip},
		},
		{
			accept:    "gzip;q=1.0, identity; q=0.5, *;q=0",
			available: brGzipPref,
			strategy:  encoding.None,
			expect:    []string{encoding.Gzip},
		},
		{
			accept:    "gzip;q=1.0, identity; q=0.5, *;q=0",
			available: brGzipPref,
			strategy:  encoding.AliasIdentity,
			expect:    []string{encoding.Gzip},
		},
		{
			accept:    "gzip;q=1.0, identity; q=0.5, *;q=0",
			available: brGzipIdentityPref,
			strategy:  encoding.AliasIdentity,
			expect:    []string{encoding.Gzip, encoding.Identity},
		},
		{
			accept:    "gzip;q=1.0, identity; q=0.5, *;q=0",
			available: brGzipIdentityPref,
			strategy:  encoding.None,
			expect:    []string{encoding.Gzip, encoding.Identity},
		},
		{
			accept:    "gzip;q=1.0, *;q=0.5",
			available: brGzipIdentityPref,
			strategy:  encoding.UseServerPref,
			expect:    []string{encoding.Gzip, encoding.Brotli, encoding.Identity},
		},
	}
	for _, tc := range testCases {
		tc := tc
		name := fmt.Sprintf("accept:(%s) | available:%v | strategy:(%v)", tc.accept, tc.available, tc.strategy)
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := tc.available.Negotiate(tc.accept, tc.strategy)
			ErrorIs(t, err, nil)
			DeepEqual(t, tc.expect, got)
		})
	}
}

func TestParseHeader(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		expectedError error
		expect        encoding.Preference
		accept        string
	}{
		{
			accept: "br",
			expect: encoding.Preference{encoding.Brotli: 1.0},
		},
		{
			accept: "gzip;q=1.0, br;q=0.1",
			expect: encoding.Preference{
				encoding.Gzip:   1.0,
				encoding.Brotli: 0.1,
			},
		},
		{
			accept: "gzip;q=1.0, identity; q=0.6, *;q=0",
			expect: encoding.Preference{
				encoding.Gzip:     1.0,
				encoding.Identity: 0.6,
				encoding.Any:      0,
			},
		},
		{
			accept:        "gzip;q=foo, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
		{
			accept:        "gzip;foo, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
		{
			accept:        "gzip;q=1;q=0.5, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
		{
			accept:        "gzip;q=, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
		{
			accept:        "gzip;, identity; q=0.5, *;q=0",
			expectedError: encoding.ErrMalformedWeight,
		},
	}
	for _, tc := range testCases {
		tc := tc
		name := fmt.Sprintf("accept:(%s) | expect:%v", tc.accept, tc.expect)
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := encoding.ParseHeader(tc.accept)
			ErrorIs(t, err, tc.expectedError)
			DeepEqual(t, tc.expect, got)
		})
	}
}
