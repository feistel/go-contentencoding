package encoding

// A Weight is the equivalent of a quality values in the HTTP spec.
//
// It is used to assign a relative "weight" to the preference for that associated kind of content.
// See https://httpwg.org/specs/rfc7231.html#quality.values
type Weight float32

// A Preference holds a list of encodings with their Weight.
type Preference map[string]Weight

// A WildcardResolutionStrategy is a strategy to resolve a wildcard character during content negotiation.
type WildcardResolutionStrategy int

const (
	// None doesn't apply any transformation and returns '*'.
	None WildcardResolutionStrategy = iota

	// AliasIdentity replaces '*' with 'Identity'.
	AliasIdentity

	// UseServerPref uses the server Preference to resolve the wildcard.
	UseServerPref
)
