package encoding_test

import (
	"errors"
	"reflect"
	"testing"
)

func ErrorIs(tb testing.TB, err, target error) {
	tb.Helper()

	if !errors.Is(err, target) {
		if target == nil {
			tb.Fatalf("received unexpected error:\n%v", err)
		}

		tb.Fatalf(`target error should be in err chain:\n
		expected: %v\n
		in chain: %v`, err, target)
	}
}

func DeepEqual(tb testing.TB, expected, actual interface{}) {
	tb.Helper()

	if !reflect.DeepEqual(expected, actual) {
		tb.Fatalf(`not equal:\n
		expected: %v\n
		actual  : %v`, expected, actual)
	}
}
