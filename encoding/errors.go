package encoding

import "errors"

var (
	// ErrEmptyHeader is returned when doing content negotiation with an empty header.
	ErrEmptyHeader = errors.New("empty header")

	// ErrMalformedWeight is returned when the Weight value is malformed.
	ErrMalformedWeight = errors.New("malformed weight")
)
