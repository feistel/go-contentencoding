package encoding

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	// A reasonable default size to avoid allocating on the heap
	// when possible.
	//
	// We know there are a fixed number of encodings:
	//
	// 'All content-coding values are case-insensitive and ought
	// to be registered within the 'HTTP Content Coding Registry'.'
	//
	// See https://httpwg.org/specs/rfc7231.html#content.codings
	// See https://httpwg.org/specs/rfc7231.html#content.coding.registry
	// See https://www.iana.org/assignments/http-parameters/http-parameters.xhtml
	acceptedEncodingsSize = 7

	// The default Q value of an encoding.
	//
	// 'If no "q" parameter is present, the default weight is 1.'
	//
	// See https://httpwg.org/specs/rfc7231.html#quality.values
	defaultWeight = Weight(1.0)
)

// Negotiate returns a slice of accepted content encodings for the
// request's Accept-Encoding header.
// The offer earlier in the list is preferred. If no offers are
// acceptable, then an empty slice is returned.
func (pref Preference) Negotiate(acceptHeader string, wildcardStrategy WildcardResolutionStrategy) ([]string, error) {
	results := make([]string, 0, acceptedEncodingsSize)

	return pref.negotiateEncodings(acceptHeader, wildcardStrategy, results)
}

//nolint:lll // internal method
func (pref Preference) negotiateEncodings(acceptHeader string, wildcardStrategy WildcardResolutionStrategy, results []string) ([]string, error) {
	if acceptHeader == "" {
		return nil, ErrEmptyHeader
	}

	acceptedEncodings, err := ParseHeader(acceptHeader)
	if err != nil {
		return nil, err
	}

	pref.resolveWildcard(acceptedEncodings, wildcardStrategy)
	results = pref.negotiate(acceptedEncodings, results)

	return results, nil
}

func (pref Preference) resolveWildcard(acceptedEncodings Preference, strategy WildcardResolutionStrategy) {
	switch strategy {
	case None:
		return
	case AliasIdentity:
		wQ, wOk := acceptedEncodings[Any]
		if !wOk {
			return
		}

		delete(acceptedEncodings, Any)

		if _, iOk := acceptedEncodings[Identity]; !iOk {
			acceptedEncodings[Identity] = wQ
		}
	case UseServerPref:
		wQ, wOk := acceptedEncodings[Any]
		if !wOk {
			return
		}

		delete(acceptedEncodings, Any)

		for k := range pref {
			if _, ok := acceptedEncodings[k]; !ok {
				acceptedEncodings[k] = wQ
			}
		}
	}
}

//nolint:cyclop // it's not too complex, there are comments and the logic is reasonable.
func (pref Preference) negotiate(acceptedEncodings Preference, results []string) []string {
	for k, v := range acceptedEncodings {
		if q, ok := pref[k]; v != 0 && ok && q != 0 {
			results = append(results, k)
		}
	}

	// Identity is always accepted unless it's explicitly excluded.
	if siQ, siOk := pref[Identity]; siOk && siQ != 0 {
		_, iOk := acceptedEncodings[Identity]
		_, aOk := acceptedEncodings[Any]

		if !iOk && !aOk {
			results = append(results, Identity)
		}
	}

	// '*' is special, servers don't need to explicitly support it.
	if q, ok := acceptedEncodings[Any]; ok && q != 0 {
		results = append(results, Any)
	}

	pref.sort(results, acceptedEncodings)

	return results
}

func (pref Preference) sort(arr []string, accepted Preference) {
	i := 1
	for i < len(arr) {
		j := i
		//nolint:lll // too long but it's fine
		for j > 0 && (accepted[arr[j-1]] < accepted[arr[j]] || (accepted[arr[j-1]] == accepted[arr[j]] && pref[arr[j-1]] < pref[arr[j]])) {
			arr[j], arr[j-1] = arr[j-1], arr[j]
			j--
		}
		i++
	}
}

// ParseHeader parse Accept-Encoding headers.
func ParseHeader(acceptHeader string) (Preference, error) {
	acceptedEncodings := make(Preference, acceptedEncodingsSize)

	return parseHeader(acceptHeader, acceptedEncodings)
}

func parseHeader(acceptHeader string, acceptedEncodings Preference) (Preference, error) {
	start, end := split(acceptHeader, 0, ',')

	for {
		h := acceptHeader[start:end]
		startC, endC := split(h, 0, ';')
		coding := strings.TrimSpace(h[startC:endC])
		w := defaultWeight

		if endC != len(h) {
			q := h[endC+1:]

			f, err := parseQ(q)
			if err != nil {
				return nil, err
			}

			w = Weight(f)
		}

		acceptedEncodings[coding] = w

		if end >= len(acceptHeader) {
			break
		}

		start, end = split(acceptHeader, end+1, ',')
	}

	return acceptedEncodings, nil
}

func parseQ(input string) (float64, error) {
	q := strings.TrimSpace(input)
	if !strings.HasPrefix(q, "q=") {
		return 0, fmt.Errorf("missing q= prefix (%s): %w", q, ErrMalformedWeight)
	}

	f, err := strconv.ParseFloat(q[2:], 32)
	if err != nil {
		return 0, fmt.Errorf("parsing error (%s): %w", q, ErrMalformedWeight)
	}

	return f, nil
}

func split(foo string, start int, b byte) (int, int) {
	end := strings.IndexByte(foo[start:], b)
	if end == -1 {
		return start, len(foo)
	}

	return start, start + end
}
